require "test_helper"

class MovieTest < ActiveSupport::TestCase
  test "is not valid without imdb_id" do
    movie = Movie.new(imdb_id: nil)
    assert_not movie.save, "Save without imdb_id"
  end

  test "is valid with imdb_id" do
    movie = Movie.new(imdb_id: 'tt2820852')
    assert movie.save, "Save with imdb_id"
  end
end