require "test_helper"

class ScheduleTest < ActiveSupport::TestCase
  test "is not valid without dt" do
    schedule = Schedule.new(price: 100, movie_id: movies("The_Fast_and_the_Furious"))
    assert_not schedule.save, "Save without dt"
  end

  test "is not valid without price" do
    schedule = Schedule.new(dt: DateTime.now, movie_id: movies("The_Fast_and_the_Furious"))
    assert_not schedule.save, "Save without price"
  end

  test "is not valid without movie" do
    schedule = Schedule.new(price: 100, dt: DateTime.now)
    assert_not schedule.save, "Save without movie"
  end
end