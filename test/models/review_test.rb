require "test_helper"

class ReviewTest < ActiveSupport::TestCase
  test "is not valid without movie" do
    review = Review.new(rating: 5)
    assert_not review.save, "Save without movie"
  end

  test "is not valid without rating" do
    review = Review.new(movie: movies("The_Fast_and_the_Furious"))
    assert_not review.save, "Save without rating"
  end

  test "is valid with movie and rating" do
    review = Review.new(movie: movies("The_Fast_and_the_Furious"), rating: 5)
    assert review.save, "Saved successfully"
  end

  test "is not valid with rating < 1" do
    review = Review.new(movie: movies("The_Fast_and_the_Furious"), rating: 0)
    assert_not review.save, "Saved with low rating"
  end

  test "is not valid with rating > 5" do
    review = Review.new(movie: movies("The_Fast_and_the_Furious"), rating: 6)
    assert_not review.save, "Saved with high rating"
  end
end
