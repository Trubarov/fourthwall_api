Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  namespace :api do
    resources :movies, only: [:index, :create, :show]

    resources :schedules, only: [:index, :update, :create, :show]

    resources :reviews, only: :create
  end
end
