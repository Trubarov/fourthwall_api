include ActionController::HttpAuthentication::Basic::ControllerMethods

class Api::ApplicationController < ActionController::API

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      ActiveSupport::SecurityUtils.secure_compare(username, ENV.fetch('AUTH_USER')) &&
          ActiveSupport::SecurityUtils.secure_compare(password, ENV.fetch('AUTH_PASSWORD'))
    end
  end
end
