class Api::ReviewsController < Api::ApplicationController
  def create
    @movie = Movie.find_by(imdb_id: params[:imdb_id])
    if @movie
      review = @movie.rate(params[:rating])
      if review.valid?
        render json: review, status: :ok
      else
        render json: { errors: review.errors }, status: :unprocessable_entity
      end
    else
      render status: :not_found
    end
  end
end
