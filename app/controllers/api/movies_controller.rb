class Api::MoviesController < Api::ApplicationController
  before_action :authenticate, only: :create

  def index
    render json: Movie.all
  end

  def create
    @movie = Movie.new(params.require(:movie).permit(:imdb_id))
    if @movie.save
      render json: @movie, status: :created
    else
      render json: { errors: @movie.errors }, status: :unprocessable_entity
    end
  end

  def show
    @movie = Movie.find_by(imdb_id: params[:id])
    if @movie
      render json: @movie, serializer: MovieExtendedSerializer
    else
      render status: :not_found
    end
  end
end
