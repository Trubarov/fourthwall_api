class Api::SchedulesController < Api::ApplicationController
  before_action :authenticate, only: [:create, :update]

  def index
    render json: Schedule.actual
  end

  def create
    schedule = Schedule.new(schedule_params.merge(movie_id: movie.id))
    if schedule.save
      render json: schedule, status: :created
    else
      render json: { errors: schedule.errors }, status: :unprocessable_entity
    end
  end

  def update
    schedule = Schedule.find(params[:id])
    if schedule.update(schedule_params.merge(movie_id: movie.id))
      render json: schedule, status: :ok
    else
      render json: { errors: schedule.errors }, status: :unprocessable_entity
    end
  end

  private

  def movie
    Movie.find_by(imdb_id: schedule_params[:movie_id])
  end

  def schedule_params
    params.require(:schedule).permit(:movie_id, :dt, :price)
  end
end
