class ReviewSerializer < ActiveModel::Serializer
  attributes :rating

  belongs_to :movie, serializer: MovieSerializer
end
