class ScheduleSerializer < ActiveModel::Serializer
  attributes :id, :dt, :price

  belongs_to :movie, serializer: MovieSerializer
end
