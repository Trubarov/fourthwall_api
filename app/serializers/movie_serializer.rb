class MovieSerializer < ActiveModel::Serializer
  attributes :imdb_id, :title
end
