class MovieExtendedSerializer < ActiveModel::Serializer
  attributes :imdb_id, :title, :omdb

  def omdb
    {
        title: object.omdb_data.title,
        plot: object.omdb_data.plot,
        released: object.omdb_data.released,
        imdb_rating: object.omdb_data.imdb_rating,
        imdb_votes: object.omdb_data.imdb_votes,
        runtime: object.omdb_data.runtime,
        metascore: object.omdb_data.metascore,
    }
  end
end
