class OmdbClient
  include Singleton

  def client
    @client ||= Omdb::Api::Client.new(api_key: ENV.fetch('OMDB_API_KEY'))
  end

  def find(imdb_id)
    client.find_by_id(imdb_id)
  end
end
