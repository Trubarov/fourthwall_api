class Movie < ApplicationRecord
  has_many :reviews

  validates :imdb_id, presence: true, uniqueness: true
  validates :title, presence: true

  before_validation :set_title, on: :create

  def rate(rating)
    self.reviews.create(rating: rating)
  end

  def omdb_data
    @omdb_data ||= OmdbClient.instance.find(self.imdb_id)
  end

  private

  def set_title
    return false if self.imdb_id.blank?
    self.title = omdb_data.title
  end
end
