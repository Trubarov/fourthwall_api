class Schedule < ApplicationRecord
  belongs_to :movie

  validates :dt, presence: true
  validates :price, presence: true
  validates :movie, presence: true

  def self.actual
    Schedule.where('dt >= ?', DateTime.now)
  end
end
