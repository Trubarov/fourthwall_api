# How to run application

## 1. Prepare application
Application runs with ruby 3.0.1. It is very simple to install required version with rbenv tool with plugin rbenv-vars to manage environment variables. Example for MacOS:
```
brew upgrade ruby-build
brew install rbenv
rbenv install 3.0.1
git clone https://github.com/rbenv/rbenv-vars.git $(rbenv root)/plugins/rbenv-vars
```

Run following command to install apllication environment:
```
bundle
```


## 2. Set up database
I used PostgreSQL 13.3
To set up database run following commands in PostgreSQL console via superuser:
```
create user "testapi" password 'your_password' login;

alter user testapi createdb;
```

## 3. Prepare environment
Create `.rbenv-vars` file and fill up database connection strings and OMDB API key. Example is in file `.rbenv-vars.example` 

Fill database with following commands:
```
rails db:setup
rails db:migrate
rails db:seed
```

## 4. Run application
```
rails s
```
Base url: http://localhost:3000/  
Swagger documentation: http://localhost:3000/api-docs/

## 5. Run tests
Unit tests:
```
rails test
```

Integration tests:
```
rspec
```

## 6. Generate Swagger documentation
```
RAILS_ENV=test rake rswag:specs:swaggerize
```

## 7. Other
For protection internal endpoints I used simple basic authorization. Credentials are set up in environment variables AUTH_USER and AUTH_PASSWORD
