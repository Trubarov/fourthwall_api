class CreateReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :reviews do |t|
      t.references :movie, index: true, foreign_key: true, null: false
      t.integer :rating
      t.timestamps
    end
  end
end
