class CreateSchedules < ActiveRecord::Migration[6.1]
  def change
    create_table :schedules do |t|
      t.references :movie, index: true, foreign_key: true, null: false
      t.datetime :dt, null: false
      t.integer :price, null: false
      t.timestamps
    end
  end
end
