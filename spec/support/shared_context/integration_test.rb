require 'rails_helper'

shared_context 'with integration test' do
  run_test!

  after do |example|
    example.metadata[:response][:content] =
        { 'application/json' => { 'example' => JSON.parse(response.body, symbolize_names: true) } }
  end
end

shared_context 'with authorization' do
  auth = ::Base64.strict_encode64("#{ENV.fetch('AUTH_USER')}:#{ENV.fetch('AUTH_PASSWORD')}")
  let(:Authorization) { "Basic #{auth}" }
end
