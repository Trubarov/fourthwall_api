require 'swagger_helper'

describe 'Movies API' do

  path '/api/movies' do

    post 'Creates a movie' do
      tags 'Movies'
      consumes 'application/json'
      parameter name: :movie, in: :body, schema: {
          type: :object,
          properties: {
              movie: {
                  type: :object,
                  properties: {
                      imdb_id: { type: :string }
                  },
                  required: ['imdb_id']
              }
          }
      }
      security [ basic_auth: [] ]

      response '201', 'Movie created' do
        include_context 'with authorization'
        let(:movie) { { movie: { imdb_id: 'tt0322258' } } }
        include_context 'with integration test'
      end

      response '422', 'invalid request' do
        include_context 'with authorization'
        let(:movie) { { movie: { imdb_id: nil } } }
        include_context 'with integration test'
      end
    end

    get 'List of movies' do
      tags 'Movies'
      consumes 'application/json'

      response '200', 'Movies' do
        schema type: :array,
               items: {
                   properties: {
                       imdb_id: { type: :string },
                       title: { type: :string }
                   }
               }
        let!(:id) { Movie.create(imdb_id: 'tt0322258') }
        include_context 'with integration test'
      end
    end


  end

  path '/api/movies/{imdb_id}' do
    get 'Show movie info' do
      tags 'Movies'
      consumes 'application/json'
      parameter name: :imdb_id, :in => :path, :type => :string

      response '200', 'Movie' do
        schema type: :object,
               properties: {
                   imdb_id: { type: :string },
                   title: { type: :string },
                   omdb: {
                       type: :object,
                       properties: {
                           title: { type: :string },
                           plot: { type: :string },
                           released: { type: :string },
                           imdb_rating: { type: :string },
                           imdb_votes: { type: :string },
                           runtime: { type: :string },
                           metascore: { type: :string }
                       }
                   }
               }
        let(:imdb_id) { Movie.create(imdb_id: 'tt0322258').imdb_id }
        include_context 'with integration test'
      end
    end
  end
end