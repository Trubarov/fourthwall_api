require 'swagger_helper'

describe 'Reviews API' do

  path '/api/reviews' do
    post 'Post review rating' do
      tags 'Reviews'
      consumes 'application/json'
      parameter name: :review, in: :body, schema: {
          type: :object,
          properties: {
              imdb_id: { type: :string },
              rating: { type: :integer }
          }
      }

      response '200', 'Review posted' do
        schema type: :object,
               properties: {
                   rating: { type: :integer },
                   movie: {
                       type: :object,
                       properties: {
                           imdb_id: { type: :string },
                           title: { type: :string }
                       }
                   }
               }
        let(:review) { { imdb_id: Movie.create(imdb_id: 'tt0322258').imdb_id, rating: 5 } }

        include_context 'with integration test'
      end
    end
  end
end