require 'swagger_helper'

describe 'Schedules API' do

  path '/api/schedules' do

    get 'List of schedules' do
      tags 'Schedules'
      consumes 'application/json'

      response '200', 'Schedules' do
        schema type: :array,
               items: {
                   properties: {
                       id: { type: :integer },
                       dt: { type: :string },
                       price: { type: :integer },
                       movie: {
                           type: :object,
                           properties: {
                               imdb_id: { type: :string },
                               title: { type: :string }
                           }
                       }
                   }
               }
        let!(:id) do
          movie_id = Movie.create(imdb_id: 'tt0322258').id
          Schedule.create(movie_id: movie_id,
                          dt: DateTime.now + 1.day,
                          price: 500)
        end

        include_context 'with integration test'
      end
    end

    post 'Creates a schedule' do
      tags 'Schedules'
      consumes 'application/json'
      parameter name: :schedule, in: :body, schema: {
          type: :object,
          properties: {
              movie_id: { type: :integer },
              dt: { type: :string },
              price: { type: :integer }
          }
      }
      security [ basic_auth: [] ]

      response '201', 'Schedule created' do
        include_context 'with authorization'
        let(:schedule) do
          {
              movie_id: Movie.create(imdb_id: 'tt0322258').imdb_id,
              dt: DateTime.now + 1.day,
              price: 500
          }
        end

        include_context 'with integration test'
      end

      response '422', 'invalid request' do
        include_context 'with authorization'
        let(:schedule) do
          {
              movie_id: Movie.create(imdb_id: 'tt0322258').imdb_id,
              dt: DateTime.now + 1.day
          }
        end
        include_context 'with integration test'
      end
    end
  end

  path '/api/schedules/{id}' do
    patch 'Update schedule' do
      tags 'Schedules'
      consumes 'application/json'
      parameter name: :schedule, in: :body, schema: {
          type: :object,
          properties: {
              movie_id: { type: :integer },
              dt: { type: :string },
              price: { type: :integer }
          }
      }
      parameter name: :id, in: :path, type: :integer
      security [ basic_auth: [] ]

      response '200', 'Schedule updated' do
        include_context 'with authorization'
        let(:sch) do
          Schedule.create(
              movie_id: Movie.create(imdb_id: 'tt1905041').id,
              dt: DateTime.now + 1.day,
              price: 500
          )
        end

        let(:schedule) do
          {
            movie_id: sch.movie.imdb_id,
            dt: sch.dt,
            price: sch.price
          }
        end
        let(:id) { sch.id }

        include_context 'with integration test'
      end
    end
  end
end
